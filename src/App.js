// import './App.css';
import MainComponent from './components/MainComponent';
import NavbarComponent from './components/NavbarComponent';

export default function App() {
  return (
    <div className='container'>
      <NavbarComponent />
      <MainComponent />
    </div>
  );
}
