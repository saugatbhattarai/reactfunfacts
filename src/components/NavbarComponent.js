
export default function NavbarComponent(){
    return(
        <div className="navbar">
            <div className="nav-logo-name">
                <img width="80px" src="./images/logo.png" className="logo-react" alt="react logo"/>
                <h2 className="company-name">ReactFacts</h2>
            </div>
            <div className="nav-title">
                <h4>React Course - Project 1</h4>
            </div>
        </div>
    );
}