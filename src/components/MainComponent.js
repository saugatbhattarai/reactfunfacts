export default function MainComponent(){
    return(
        <div className="main-content">
            <div className="content">
                <h1>Fun Facts about React</h1>
                <ul>
                    <li>Was first released in 2013</li>
                    <li>Was originally created by Jordan Walke</li>
                    <li>Has Well over 100k starts on Github</li>
                    <li>Is maintained by Facebook</li>
                    <li>Powers thousands of enterprise apps, including mobile apps</li>
                </ul>
            </div>
            <div className="background-logo">
                <img src="./logo192.png" alt="background logo" />
            </div>
        </div>
    );
}